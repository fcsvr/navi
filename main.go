package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"github.com/gin-gonic/gin"
)

func httpServer() {
	router := gin.Default()
	router.LoadHTMLGlob("view/*")
	router.StaticFS("/ui", http.Dir("ui"))
	router.GET("/", index)
	router.GET("/add", add)
	router.POST("/add_post", add_post)
	router.RunTLS(":7001", "/root/.ssl/ssl.pem", "/root/.ssl/ssl.key")
}

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "FancyGo Web",
	})
}

func add(c *gin.Context) {
	c.HTML(http.StatusOK, "add.html", gin.H{
		"title": "add_url",
	})
}

func add_post(c *gin.Context) {
	kind := c.PostForm("kind")
	url := c.PostForm("url")
	name := c.PostForm("name")
	desc := c.PostForm("desc")
	fmt.Println(kind, url, name, desc)
	c.IndentedJSON(http.StatusOK, true)
}

func main() {
	fmt.Println("FancyGo Web Navi")

	go httpServer()

	close := make(chan os.Signal, 1)
	signal.Notify(close, os.Interrupt, os.Kill)
	<-close

	fmt.Println("Bye, FancyGo Web Navi Close")
}
